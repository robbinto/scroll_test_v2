This repository is a programming challenge by The Secret Police. To get started please **Fork** the repository into your own BitBucket account before making any commits. When you have finished your solution please create **Pull Request** to the original repository or a link to your repository.

It contains a Unity project (2017.2.0p4) written in C#. The test requires that you create Android builds, so please makes sure you have the Android SDK installed.

Open the Main Menu scene and run it in the IDE.
You should see a scroll view with 1000 items in that can be scrolled vertically.
Each item contains an image button and a text object.
Each image is unique on purpose.

If you do an Android build, you'll notice that the scrolling performance is really bad. We have included an FPS UI component in the Main Menu scene.

We would like you to come up with a better solution than Unity's default Scroll View.

Things to consider:
* maybe you don't need to instantiate 1000 prefabs,
* maybe you don't need all those Rect Transforms,
* minimising draw calls

We are looking for an optimal solution and would like you to make an Android build of the MainMenu scene that's super smooth.

Feel free to ask us questions as you go.