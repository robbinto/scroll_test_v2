﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ThumbnailManager:MonoBehaviour {
	
	struct GridData
	{
		public Vector2 cellSize;
		public Vector2Int cellCount;
		public Vector2 gridSize;
	}

	public Transform container;
	public GameObject prefab;

	private List<ThumbnailVO> _thumbnailVOList = new List<ThumbnailVO>();

	private List<Thumbnail> _thumbnailObjects = new List<Thumbnail>();

	private GridData _gridData = new GridData();

	private int _topRowLastFrame = 0;
	
	void Start () {
		calcGridData();
		
		createThumbnailVOList();
		createThumbnailPrefabs();
    }

	void Update() {
		//PERF: It may be possible to optimize performance by rearranging the rows instead of rendering enough of them 
		//		in each direction (right now 2/3 rows are outside the screen at any given time). I don't have the time 
		//		to look into that now though.

		int scrolledRows = (int)((container.transform.localPosition.y - _gridData.gridSize.y / 3f) / _gridData.cellSize.y);
		int topRow = _topRowLastFrame + scrolledRows;
		
		if(!Input.GetMouseButton(0) && scrolledRows != 0 && topRow >= 0 && topRow + _gridData.cellCount.y <= _thumbnailVOList.Count / 5)
		{
			for(int i = 0; i < _thumbnailObjects.Count; i++)
			{
				_thumbnailObjects[i].thumbnailVO = _thumbnailVOList[i + topRow * _gridData.cellCount.x];
			}

			container.transform.localPosition -= Vector3.up * scrolledRows * _gridData.cellSize.y;

			_topRowLastFrame = topRow;
		}
	}

	private void calcGridData()
	{
		GridLayoutGroup gridLayoutGroup = container.GetComponent<GridLayoutGroup>();

		_gridData.cellSize = gridLayoutGroup.cellSize + gridLayoutGroup.spacing;

		//NOTE: The scroll list doesn't reset its' position while the user is touching the screen.
		//		Covering 3x the screen space plus an extra row in each direction vertically to support scrolling 
		//		any distance in any direction. 
		_gridData.cellCount = new Vector2Int(gridLayoutGroup.constraintCount, (int)(Screen.height * 3 / _gridData.cellSize.y) + 2);

		_gridData.gridSize = _gridData.cellSize * _gridData.cellCount;
	}
	
	private void createThumbnailVOList() {
		ThumbnailVO thumbnailVO;
		for (int i=0; i<1000; i++) {
			thumbnailVO = new ThumbnailVO();
			thumbnailVO.id = i.ToString();
            _thumbnailVOList.Add(thumbnailVO);
        }
	}

	private void createThumbnailPrefabs() {
		GameObject gameObj;
		
		float yMax = Screen.height + 5f * _gridData.cellSize.y;

		for (int i=0; i / _gridData.cellCount.x < _gridData.cellCount.y && i<_thumbnailVOList.Count; i++) { 
			gameObj = (GameObject)Instantiate(prefab);
			gameObj.transform.SetParent(container, false);

			Thumbnail thumbnail = gameObj.GetComponent<Thumbnail>();
			thumbnail.thumbnailVO = _thumbnailVOList[i];

			_thumbnailObjects.Add(thumbnail);
        }
	}
}
